#include <iostream>
#include <systemc.h>
#include "CtrlMem.hpp"

using namespace std;

SC_HAS_PROCESS (CtrlMemory);

CtrlMemory::CtrlMemory (sc_module_name controlmem) : sc_module (controlmem) {
   SC_THREAD (control_address_thread);
   SC_THREAD (control_permission_r_thread);
   SC_THREAD (control_permission_w_thread);
   SC_THREAD (control_permission_d_thread);

// Inizializzo i vettori dei permessi tutti ad 1,vuol dire che in ogni postazione di memoria posso leggere,scrivere e cancellare il dato
// Inizializzo il vettore valid tutto ad 1 per cui tutti gli indirizzi sono validi
   for (int i = 0; i < 10; i++) {
      permission_r[i] = 1;
      permission_w[i] = 1;
      permission_d[i] = 1;
      valid[i] = 1;
   }
// Inizializzo le variabili di controllo
   this -> ready = 1;
   this -> error = 0;
   this -> data_ready = 0;
   this -> data_write = 0;
   this -> data_out = 0;
}
// Thread che controlla se l'indirizzo che passiamo alla memoria è corretto
void CtrlMemory::control_address_thread () {
   while (true) {
      if (this -> ready == 1) {
         this ->  addr = this -> Address -> read().to_uint();
         this -> ready = 0;
         this -> error = 0;
         this -> data_ready = 0;
         this -> data_write = 0;
         this -> data_out = 0;
         if (addr >= 10) {							//controllo indirizzo sia valido
            this -> error = 1;							//se non è valido segnalo l'errore e aggiorno le porte
            this -> address_ok = 0;
            this -> ctrl_rwd = this -> Ctrl_rwd -> read ().to_uint();
            del_trash_event.notify (SC_ZERO_TIME);
            update_port_event.notify (SC_ZERO_TIME);
         }
         else {
            this -> ctrl_rwd = this -> Ctrl_rwd -> read ().to_uint();		//leggo se voglio leggere,scrivere o eliminare il dato a 
            this -> address_ok = 1;						//quell'indirizzo
            update_port_event.notify (SC_ZERO_TIME);
         }
      }
      else
         wait (ready_event);
   }
}
// Thread che controlla se abbiamo i permessi per leggere da quella postazione di memoria e se il dato è valido
void CtrlMemory::control_permission_r_thread () {
   while (true) {
      wait (address_ok_event);
      this -> address_ok = 0;
      if (((permission_r[this -> addr]) == 1) && ((valid[this -> addr]) == 1)) { 
         read_ok_event.notify (SC_ZERO_TIME);
      }
      else {
         if (ctrl_rwd == 0) {
            this -> ready = 1;
            this -> error = 3;
            update_port_event.notify (SC_ZERO_TIME);
         }
      }
   }
}
// Thread che controlla se abbiamo i permessi per scrivere all'indirizzo indicato
void CtrlMemory::control_permission_w_thread () {
   int trash;
   while (true) {
      wait (address_ok_event);
      this -> address_ok = 0;
      if ((permission_w[this -> addr]) == 1) {
         valid[this -> addr] = 1;			//dopo aver scritto il dato lo segno come valido
         write_ok_event.notify (SC_ZERO_TIME);
      }
      else {
         if (ctrl_rwd == 1) {
            this -> ready = 1;				//se non posso scrivere a questo indirizzo segnalo l'errore
            this -> error = 2;
            update_port_event.notify (SC_ZERO_TIME);
            del_trash_event.notify (SC_ZERO_TIME);
         }
      }
   }
}
// Thread che controlla se abbiamo i permessi per eliminare il dato all'indirizzo indicato
void CtrlMemory::control_permission_d_thread () {
   while (true) {
      wait (address_ok_event);
      this -> address_ok = 0;
      if ((permission_d[addr]) == 1) {
         valid[this -> addr] = 0;			//se cancello il dato segnalo che quello che c'è in quella locazione non è più valido
         del_ok_event.notify (SC_ZERO_TIME);
      }
      else {
         if (ctrl_rwd == 2) {
            this -> ready = 1;				//se non posso eliminare quel dato segnalo l'errore
            this -> error = 4;
            update_port_event.notify (SC_ZERO_TIME);
         }
      }
   }
}
