#include <iostream>
#include <systemc.h>
#include "Mem.hpp"

using namespace std;

SC_HAS_PROCESS (Memory);

Memory::Memory (sc_module_name mem) : sc_module (mem), ctrlmem ("mem") {
   SC_THREAD (initialize);
   SC_THREAD (read_thread);
   SC_THREAD (write_thread);
   SC_THREAD (del_thread);
   SC_THREAD (write_port_thread);
   SC_THREAD (del_trash_thread);

   ctrlmem.Ctrl_rwd (this -> rwd);
   ctrlmem.Address (this -> Address_mem);
// Inizializzo la memoria scrivendo 0 in tt le postazioni
   for (int i = 0; i < 10; i++)
      mem_array[i] = 0;

}

// Thread eseguito solo una volta per inizializzare le porte della memoria
void Memory::initialize () {
   this -> Data_out -> write (ctrlmem.data_out);
   this -> Data_write -> write (ctrlmem.data_write);
   this -> Data_ready -> write (ctrlmem.data_ready);
   this -> Error -> write (ctrlmem.error);
   this -> Ready -> write (ctrlmem.ready);
   initialize_event.notify (SC_ZERO_TIME);
}

// Thread che legge il dato dall'indirizzo desiderato
void Memory::read_thread () {
   while (true) {
      wait (ctrlmem.read_ok_event);
      if (ctrlmem.ctrl_rwd == 0) {
         wait (40, SC_NS);                                    		// consideriamo che la memoria ci metta 40 ns a leggere il dato
         ctrlmem.data_out = mem_array[ctrlmem.addr].to_uint();		//aggiorniamo le variabili di controllo della ctrlmem
         ctrlmem.ready = 1;
         ctrlmem.data_ready = 1;
         ctrlmem.update_port_event.notify (SC_ZERO_TIME);
      }
   }
}

// Thread che scrive il dato nella posizione specificata
// Inoltre è possibile proteggere il dato da scrittura o lettura
void Memory::write_thread () {
   int pro;
   while (true) {
      wait (ctrlmem.write_ok_event);
      if (ctrlmem.ctrl_rwd == 1) {
         wait (40, SC_NS);                                    		// consideriamo che la memoria ci metta 40 ns a scrivere il dato 
         mem_array[ctrlmem.addr] = this -> data_in -> read ();		//scriviamo il dato presente sulla porta data in in memoria
         ctrlmem.ready = 1;						//e aggiorniamo le variabili di controllo della ctrlmem
         ctrlmem.data_write = 1;
         ctrlmem.update_port_event.notify (SC_ZERO_TIME);
         pro = this -> protect -> read ().to_uint();
         if (pro == 0) {						//controllo l'ingresso protect per capire se e come devo proteggere
            ctrlmem.permission_w[ctrlmem.addr] = 1;			//il dato
         }
         else if (pro == 1){
            ctrlmem.permission_w[ctrlmem.addr] = 0;
         }
         else if (pro == 2) {
            ctrlmem.permission_r[ctrlmem.addr] = 0;
         }
         else if (pro == 3) {
            ctrlmem.permission_w[ctrlmem.addr] = 0;
            ctrlmem.permission_r[ctrlmem.addr] = 0;
         }
      }
   }
}

// Thread che elimina il dato all'indirizzo indicato
void Memory::del_thread () {
   while (true) {
      wait (ctrlmem.del_ok_event);
      if (ctrlmem.ctrl_rwd == 2) {					//quando elimino il dato praticamente rimetto i permessi di lettura e
         ctrlmem.permission_w[ctrlmem.addr] = 1;			//scrittura a quella locazione di memoria
         ctrlmem.permission_r[ctrlmem.addr] = 1;
         ctrlmem.ready = 1;
         ctrlmem.update_port_event.notify (SC_ZERO_TIME);
      }
   }
}

// Thread che aggiorna le porte di uscita della memoria
void Memory::write_port_thread () {
   while (true) {
      wait (ctrlmem.update_port_event);
      if (ctrlmem.address_ok == 1)
         ctrlmem.address_ok_event.notify (SC_ZERO_TIME);
      else {
         ctrlmem.ready = 1;
         ctrlmem.ready_event.notify (SC_ZERO_TIME);
      }

      this -> Data_out -> write (ctrlmem.data_out);
      this -> Data_write -> write (ctrlmem.data_write);
      this -> Data_ready -> write (ctrlmem.data_ready);
      this -> Error -> write (ctrlmem.error);
      this -> Ready -> write (ctrlmem.ready);

      print_port_event.notify (SC_ZERO_TIME);
   }
}

// Thread che mi serve solo per la simulazione ed il test perchè devo svuotare le code
void Memory::del_trash_thread () {
   int trash;

   while (true) {
      wait (ctrlmem.del_trash_event);
      trash = this -> data_in -> read ().to_uint();
      trash = this -> protect -> read ().to_uint();
   }
}
