#include <iostream>
#include <systemc.h>
#include "testbench.hpp"

using namespace std;

SC_HAS_PROCESS (Testbench);

Testbench::Testbench (sc_module_name testbench) : sc_module (testbench), memory("memory") {
   SC_THREAD (stimulus_thread);
   SC_THREAD (test_thread);
   SC_THREAD (failed_test_thread);

   memory.Address_mem (this -> Address_signal);
   memory.rwd (this -> Rwd_signal);
   memory.data_in (this -> Data_in_signal);
   memory.protect (this -> Protect_signal);
   memory.Data_out (this -> Data_out_signal);
   memory.Data_ready (this -> Data_ready_signal);
   memory.Data_write (this -> Data_write_signal);
   memory.Error (this -> Error_signal);
   memory.Ready (this -> Ready_signal);

   result_test = 0;
}

// Thread per dare gli stimoli alla memoria per il test
void Testbench::stimulus_thread () {
   //scrivo ad un indirizzo non valido
   wait (next_step_event);
   this -> Address_signal.write("0000000000001010");
   this -> Rwd_signal.write("01");
   this -> Data_in_signal.write("0000000000010111");
   this -> Protect_signal.write("11");
   //leggo da un indirizzo non valido
   wait (next_step_event);
   this -> Address_signal.write("0000000000001011");
   this -> Rwd_signal.write("00");
   this -> Data_in_signal.write("0000000000010111");
   this -> Protect_signal.write("00");
   //scrivo 23 all'indirizzo 5 senza protezione
   wait (next_step_event);
   this -> Address_signal.write("0000000000000101");
   this -> Rwd_signal.write("01");
   this -> Data_in_signal.write("0000000000010111");
   this -> Protect_signal.write("00");
   //leggo dall'indirizzo 5
   wait (next_step_event);
   this -> Address_signal.write("0000000000000101");
   this -> Rwd_signal.write("00");
   //scrivo 70 all'indirizzo 3 e proteggo contro scrittura
   wait (next_step_event);
   this -> Address_signal.write("0000000000000011");
   this -> Rwd_signal.write("01");
   this -> Data_in_signal.write("0000000001000110");
   this -> Protect_signal.write("01");
   //leggo dall'indirizzo 3
   wait (next_step_event);
   this -> Address_signal.write("0000000000000011");
   this -> Rwd_signal.write("00");
   //scrivo 15 all'indirizzo 3
   wait (next_step_event);
   this -> Address_signal.write("0000000000000011");
   this -> Rwd_signal.write("01");
   this -> Data_in_signal.write("0000000000001111");
   this -> Protect_signal.write("00");
   //elimino dato all'indirizzo 3
   wait (next_step_event);
   this -> Address_signal.write("0000000000000011");
   this -> Rwd_signal.write("10");
   //leggo dall'indirizzo 3
   wait (next_step_event);
   this -> Address_signal.write("0000000000000011");
   this -> Rwd_signal.write("00");
   //scrivo 40 all'indirizzo 3 protetto contro lettura e scrittura
   wait (next_step_event);
   this -> Address_signal.write("0000000000000011");
   this -> Rwd_signal.write("01");
   this -> Data_in_signal.write("0000000000101000");
   this -> Protect_signal.write("11");
   //leggo dall'indirizzo 3
   wait (next_step_event);
   this -> Address_signal.write("0000000000000011");
   this -> Rwd_signal.write("00");
   //scrivo 22 all'indirizzo 3
   wait (next_step_event);
   this -> Address_signal.write("0000000000000011");
   this -> Rwd_signal.write("01");
   this -> Data_in_signal.write("0000000000010110");
   this -> Protect_signal.write("00");
   //leggo da indirizzo non valido
   wait (next_step_event);
   this -> Address_signal.write("0000000000001010");
   this -> Rwd_signal.write("00");
}

// Thread per raccogliere i risultati del test e verificarne la correttezza
void Testbench::test_thread () {
   int re;
   int err;
   int d_w;
   int d_r;
   int d_o;

   wait (memory.initialize_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 0) && (d_w == 0) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 1) && (d_w == 0) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 1) && (d_w == 0) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 0) && (d_w == 1) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 0) && (d_w == 0) && (d_r == 1) && (d_o == 23))
      next_step_event.notify (SC_ZERO_TIME); 
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 0) && (d_w == 1) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 0) && (d_w == 0) && (d_r == 1) && (d_o == 70))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 2) && (d_w == 0) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 0) && (d_w == 0) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 3) && (d_w == 0) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 0) && (d_w == 1) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 3) && (d_w == 0) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re != 0) || (err != 0) || (d_w != 0) || (d_r != 0) || (d_o != 0))
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 2) && (d_w == 0) && (d_r == 0) && (d_o == 0))
      next_step_event.notify (SC_ZERO_TIME);
   else
      failed_test_event.notify (SC_ZERO_TIME);

   wait (memory.print_port_event);
   re = this -> Ready_signal.read ().to_uint();
   err = this -> Error_signal.read ().to_uint();
   d_w = this -> Data_write_signal.read ().to_uint();
   d_r = this -> Data_ready_signal.read ().to_uint();
   d_o = this -> Data_out_signal.read ().to_uint();
   if ((re == 1) && (err == 1) && (d_w == 0) && (d_r == 0) && (d_o == 0)) 
      result_test = 0;
   else
      failed_test_event.notify (SC_ZERO_TIME);

}

// Thread che segnala se il test fallisce
void Testbench::failed_test_thread () {
   wait (failed_test_event);
   result_test = -1;
}
