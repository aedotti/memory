cmake_minimum_required (VERSION 2.6)

include_directories ("include")

add_library (memory_lib "src/CtrlMem.cpp" "src/Mem.cpp" "src/testbench.cpp")

add_executable (test_memory test/test_memory.cpp)

target_link_libraries (test_memory memory_lib systemc)

enable_testing ()

add_test (TEST_MEMORY test_memory)
