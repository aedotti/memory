#include <iostream>
#include <systemc.h>
#include "testbench.hpp"

using namespace std;

int sc_main(int argc, char* argv[])
{

   Testbench testbench ("testbench");

   sc_start();

   return testbench.result_test;
}

