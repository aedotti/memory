#ifndef MEM_HPP
#define MEM_HPP

#include <iostream>
#include <systemc.h>
#include "CtrlMem.hpp"

using namespace std;
// memoria con 10 locazioni da 16 bit
SC_MODULE (Memory) {
   sc_port<sc_fifo_in_if<sc_lv<16> > > Address_mem;
   sc_port<sc_fifo_in_if<sc_lv<2> > > rwd;
   sc_port<sc_fifo_in_if<sc_lv<16> > > data_in;
   sc_port<sc_fifo_in_if<sc_lv<2> > > protect;
   sc_port<sc_fifo_out_if<sc_lv<16> > > Data_out;
   sc_port<sc_fifo_out_if<sc_lv<1> > > Data_ready;
   sc_port<sc_fifo_out_if<sc_lv<1> > > Data_write;
   sc_port<sc_fifo_out_if<sc_lv<3> > > Error;
   sc_port<sc_fifo_out_if<sc_lv<1> > > Ready;

   sc_event initialize_event;
   sc_event print_port_event;

   void initialize ();
   void read_thread ();
   void write_thread ();
   void del_thread ();
   void write_port_thread ();
   void del_trash_thread ();

   CtrlMemory ctrlmem;

   Memory (sc_module_name mem);

public :
// Vettore di memoria
   sc_lv<16>  mem_array[10];
};

#endif

