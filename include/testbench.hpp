#ifndef TESTBENCH_HPP
#define TESTBENCH_HPP

#include <iostream>
#include <systemc.h>
#include "Mem.hpp"

using namespace std;
// testbench per poter simulare e testare la memoria
SC_MODULE (Testbench) {
   sc_fifo<sc_lv<16> > Address_signal;
   sc_fifo<sc_lv<2> > Rwd_signal;
   sc_fifo<sc_lv<16> > Data_in_signal;
   sc_fifo<sc_lv<2> > Protect_signal;
   sc_fifo<sc_lv<16> > Data_out_signal;
   sc_fifo<sc_lv<1> > Data_ready_signal;
   sc_fifo<sc_lv<1> > Data_write_signal;
   sc_fifo<sc_lv<3> > Error_signal;
   sc_fifo<sc_lv<1> > Ready_signal;

   sc_event next_step_event;
   sc_event failed_test_event;

   Memory memory;

   Testbench (sc_module_name testbench);

   void stimulus_thread ();
   void test_thread ();
   void failed_test_thread ();

   int result_test;
};

#endif

