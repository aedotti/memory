#ifndef CTRLMEM_HPP
#define CTRLMEM_HPP

#include <iostream>
#include <systemc.h>

using namespace std;

SC_MODULE (CtrlMemory) {
   sc_port<sc_fifo_in_if<sc_lv<16> > > Address;
   sc_port<sc_fifo_in_if<sc_lv<2> > > Ctrl_rwd;

   sc_event address_ok_event;
   sc_event read_ok_event;
   sc_event write_ok_event;
   sc_event del_ok_event;
   sc_event update_port_event;
   sc_event ready_event;
   sc_event del_trash_event;

   void control_address_thread ();
   void control_permission_r_thread();
   void control_permission_w_thread();
   void control_permission_d_thread();

   CtrlMemory (sc_module_name controlmem);

public:
// i prossimi 3 vettori contengono le informazioni se in una data locazione di memoria è possibile leggere,scrivere o cancellare il dato
// valid ci dice se il dato in quella locazione di memoria è valido o meno ad esempio xchè è stato cancellato
   int permission_r[10];
   int permission_w[10];
   int permission_d[10];
   int valid[10];

   int addr;
   int ctrl_rwd;
   int ready;
   int error;
   int data_ready;
   int data_write;
   int data_out;
   int address_ok;
};

#endif

